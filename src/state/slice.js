import { createSlice } from '@reduxjs/toolkit'

export const userSlice = createSlice({
    name: 'user',
    initialState: {
        username: null,
        signingIn: false
    },
    reducers: {
        signedIn: (state, action) => {
            state.username = action.payload.username;
            state.signingIn = false;
        },
        requestedSignIn: (state) => {
            state.signingIn = true;
            state.username = null;
        },
        signedOut: (state) => {
            state.username = null;
            state.signingIn = false;
        },
    },
})

// Action creators are generated for each case reducer function
export const { signedIn, requestedSignIn, signedOut } = userSlice.actions

export default userSlice.reducer
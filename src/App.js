import logo from './logo.svg';
import './App.css';

import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage.jsx';
import SignInPage from './SignInPage.jsx';
import SignUpPage from './SignUpPage.jsx';
import ProfilePage from './ProfilePage.jsx';
import Page404 from './Page404.jsx';


function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<MainPage />} />
                <Route path="signin" element={<SignInPage />} />
                <Route path="profile" element={<ProfilePage />} />
                <Route path="signup" element={<SignUpPage />} />
                <Route path="*" element={<Page404 />} />
            </Routes>
        </BrowserRouter>
    );

}

export default App;

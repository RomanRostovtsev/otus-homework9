import Button from 'react-bootstrap/Button';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux'
import { signedIn, requestedSignIn, signedOut } from './state/slice.js'
import { useNavigate } from "react-router-dom";
import Form from 'react-bootstrap/Form';

function SignInPage() {
    const dispatch = useDispatch();
    const username = useSelector(state => state.user.username);
    const signingIn = useSelector(state => state.user.signingIn);
    const navigate = useNavigate();

    if (username == null) {

        return (
            <div>
                <h1>Sign In</h1>
                <Form onSubmit={(event) => {
                    dispatch(requestedSignIn());
                    const data = {
                        "login": event.target.formEmail.value,
                        "password": event.target.formPassword.value
                    };
                    axios.post("/api/v1/Account/signin", data).then(response => {
                        dispatch(signedIn({ username: data.login }));
                        navigate("/");
                    }).catch(error => {
                        dispatch(signedOut());
                        let msg;
                        if (error.response) {
                            msg = error.response.status;
                        } else {
                            msg = error.message;
                        }
                        alert("Could not sign in: " + msg);
                    });
                    event.preventDefault(); //to avoid form data sending

                }}>
                    <fieldset disabled={signingIn}>
                        <Form.Group className="mb-3" controlId="formEmail">
                            <Form.Label column="true">Email address: </Form.Label><br></br>
                            <Form.Control type="email" placeholder="Enter email" />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formPassword">
                            <Form.Label column="true">Password: </Form.Label><br></br>
                            <Form.Control type="password" placeholder="Enter password" />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Sign In
                        </Button>
                        <Button variant="primary" type="button" onClick={event => { navigate("/") }}>Cancel</Button>
                    </fieldset>
                </Form>
            </div>
        );
    }
    else {
        return (
            <div>You have already signed in as {username}.</div>
        );
    }
};

export default SignInPage; 
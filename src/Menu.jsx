import { useSelector, useDispatch } from 'react-redux'
import { signedIn, requestedSignIn, signedOut } from './state/slice.js'
import { Link, useNavigate } from "react-router-dom";
//import { signedIn, increment } from './state/slice.js'
import Button from 'react-bootstrap/Button';

function Menu() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const username = useSelector(state => state.user.username);
    if (username == null) {
        return (
            <div>
                Hello, guest! <br></br>
                <Link to="/">Home</Link> | 
                <Link to="/signin">Sign In</Link> | 
                <Link to="/signup">Sign Up</Link>
            </div>
        );
    }
    else {
        return (
            <div>Hello, {username}!<br></br>
                <Link to="/">Home</Link> | 
                <Link to="/profile">Profile</Link> | 
                <Button as="a" onClick={(event) => { dispatch(signedOut()); navigate("/"); }} >Sign Out</Button>
            </div>
        );
    }
}

export default Menu;
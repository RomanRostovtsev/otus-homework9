import WithMenu from './WithMenu.jsx'

function Page404() {
    return (
        <div>
            <h1>404</h1>This page has been moved or deleted. We apologize for the inconvenience.
        </div>
    );
};

export default WithMenu(Page404); 
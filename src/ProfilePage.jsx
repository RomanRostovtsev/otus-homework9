import { useSelector } from 'react-redux'
import Menu from './Menu.jsx'
import WithMenu from './WithMenu.jsx'

function ProfilePage() {
    const username = useSelector(state => state.user.username);

    if (username == null) {
        return (
            <div>
                <Menu /><br></br>
                Your must be authorized to access your profile.
            </div>
        );
    }
    else {
        return (
            <div>
                <h1>Profile</h1>
                Your profile is here.
            </div>
        );
    }
}

export default WithMenu(ProfilePage);
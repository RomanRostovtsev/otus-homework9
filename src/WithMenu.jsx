import Menu from './Menu.jsx'

function WithMenu(Component) {
    return props =>
    {
        return (
            <div>
                <Menu />
                <hr></hr>
                <Component />
            </div>
        );
    }
}

export default WithMenu;
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux'
import { signedIn, requestedSignIn, signedOut } from './state/slice.js'
import { useNavigate } from "react-router-dom";


function SignUpPage() {
    const dispatch = useDispatch();
    const username = useSelector(state => state.user.username);
    const signingIn = useSelector(state => state.user.signingIn);
    const navigate = useNavigate();

    if (username == null) {
        return (
            <div>
                <h1>Sign Up</h1>
                <Form onSubmit={(event) => {
                    event.preventDefault(); //to avoid form data sending
                    const email = event.target.formEmail.value;
                    if (!email) {
                        alert("Email cannot be empty");
                        return;
                    }
                    const psw = event.target.formPassword.value;
                    const psw2 = event.target.formPasswordConfirm.value;
                    if (psw != psw2) {
                        alert("Passwords do not match.");
                        return;
                    }
                    const data = {
                        "email": email,
                        "password": psw,
                        "confirmPassword": psw2,
                        "lastName": event.target.formSurname.value,
                        "firstName": event.target.formName.value,
                        "secondName": "",
                        "isTeacher": false                        
                    };
                    dispatch(requestedSignIn());
                    axios.post("/api/v1/Account/signup", data).then(response => {
                        dispatch(signedIn({ username: data.email }));
                        navigate("/");
                    }).catch(error => {
                        dispatch(signedOut());
                        let msg;
                        if (error.response) {
                            msg = error.response.status;
                        } else {
                            msg = error.message;
                        }
                        alert("Could not sign up: " + msg);
                    });


                }}>
                    <fieldset disabled={signingIn}>
                        <Form.Group className="mb-3" controlId="formEmail">
                            <Form.Label column="true">Email address: </Form.Label><br></br>
                            <Form.Control type="email" placeholder="Enter email" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formName">
                            <Form.Label column="true">Name: </Form.Label><br></br>
                            <Form.Control type="text" placeholder="Enter your name" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formSurname">
                        <Form.Label column="true">Surname: </Form.Label><br></br>
                            <Form.Control type="text" placeholder="Enter your surname" />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formPassword">
                        <Form.Label column="true">Password: </Form.Label><br></br>
                            <Form.Control type="password" placeholder="Enter password" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formPasswordConfirm">
                            <Form.Label column = "true">Confirm password: </Form.Label><br></br>
                            <Form.Control type="password" placeholder="Enter password again" />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Sign Up
                        </Button>
                        <Button variant="primary" type="button" onClick={event => { navigate("/") }}>Cancel</Button>
                    </fieldset>
                </Form>
            </div>
        );
    }
    else {
    return (
        <div>You have already signed in as {username}.</div>
    );
}

};

export default SignUpPage; 